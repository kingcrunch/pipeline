# Yaml

## `yamllint`

Lint all yaml files using
[YAML Lint](https://yamllint.readthedocs.io/en/stable/).

Toolchain: [YAML Lint](https://yamllint.readthedocs.io/en/stable/).
Enable: By files `.yamllint.yml`
Configuration: By files `.yamllint.yml`
Fails: On Merge Request into default branch, on push into default branch, on tag
Depends on: Nothing
Required by: Nothing

Example:

```yaml
---
extends: default
```
