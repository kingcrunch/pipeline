# Editorconfig

## `editorconfig-checker`

Check all files based on [editorconfig](https://editorconfig.org/) settings.

Toolchain: [editorconfig-checker](https://github.com/editorconfig-checker/editorconfig-checker).
Enable: By files `.editorconfig`
Configuration: By files `.editorconfig`
Fails: On Merge Request into default branch, on push into default branch, on tag
Depends on: Nothing
Required by: Nothing

Example:

```ini
root = true
```
