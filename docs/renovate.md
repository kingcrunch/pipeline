# Renovate

Integrates [renovate](https://www.mend.io/free-developer-tools/renovate/) as
manual job into the pipeline.

## Enable and configure

The `renovate` configuration must exist.

- `renovate.json`

See [docs.renovatedoc.com](https://docs.renovatebot.com/).

```json
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": [
    "config:base"
  ]
}
```

Additionally, when triggered the variable `RENOVATE` must be set to a no-empty
value.

## Workflow

This feature consists of a single job, that runs manually only. `web` and
`schedule` are supported as trigger and `RENOVATE` must be set to a non-empty
value.

| Stage | `test`                      | `apply`    |
|-------|-----------------------------|------------|
|       | `renovate:config-validator` | `renovate` |
