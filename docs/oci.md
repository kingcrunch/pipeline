# OCI

Build and publish OCI-compatible images.

Aside from publishing images for use by other system, this image can be used
by other features for example for deployment, or further investigation, or
usage.

## Requirements

- One, or more Dockerfiles
  - `Dockerfile`
  - `docker/**Dockerfile`
  - `docker/**/*.Dockerfile`
- The container registry is enabled, when it should be used as build-, or
  publish repository.
  - `$CI_REGISTRY || $OCI_BUILD_REPOSITORY_ROOT != $CI_REGISTRY`
  - `$CI_REGISTRY || $OCI_PUBLISH_REPOSITORY_ROOT != $CI_REGISTRY`

The location and name of the `Dockerfile`s also defines, how the resulting
images will be named.

## Files

- `Dockerfile`, `docker/**/Dockerfile`, `docker/**/*.Dockerfile`: Image build
  scripts used to [build the images](#job-ocibuild)
- `.hadolint.yaml` (optional): Configuration used by [`hadolint`](#job-ocilinthadolint)

## Variables

- `OCI_BUILD_REPOSITORY_ROOT`:
  - Default: `$CI_PROJECT_IMAGE`
  - Description: Repository root, which will be used during build. When
    possible, this repository should be near the runners.
- `OCI_PUBLISH_REPOSITORY_ROOT`
  - Default: `$OCI_BUILD_REPOSITORY_ROOT`
  - Description: Repository root, where the final images will be published.
- `OCI_PUBLISH_VOLATILE_IMAGES`
  - Default:
  - Description: When set, the volatile images will be published too right after
    they are build independent of any other build status.

## Description

| `build`                      | `test`                | `publish`                |
|------------------------------|-----------------------|--------------------------|
| [`oci:build`](#job-ocibuild) | [`oci:lint:dockle`]   | [`oci:publish:volatile`] |
|                              | [`oci:lint:hadolint`] | [`oci:publish:branch`]   |
|                              |                       | [`oci:publish:tag`]      |

### Job `oci:build`

Collect dockerfiles and builds volatile images based on the
path- and filename of the files it found. Every image will be prefixed with
`OCI_BUILD_REPOSITORY_ROOT` and will be tagged with the shortened sha-hash of
the git commit.

| Filename                    | Resulting image                                    |
|-----------------------------|----------------------------------------------------|
| `docker/Dockerfile`         | `OCI_BUILD_REPOSITORY_ROOT:SHORT_SHA_HASH`         |
| `docker/foo/bar/Dockerfile` | `OCI_BUILD_REPOSITORY_ROOT/foo/bar:SHORT_SHA_HASH` |
| `docker/bar/foo.Dockerfile` | `OCI_BUILD_REPOSITORY_ROOT/bar/foo:SHORT_SHA_HASH` |

### Job `oci:lint:dockle`

Use [`dockle`](https://github.com/goodwithtech/dockle) to lint images.

### Job `oci:lint:hadolint`

use [`hadolint`](https://github.com/hadolint/hadolint) to lint `Dockerfile`s.

- Configuration: `.hadolint.yaml`

### Job `oci:publish:volatile`

- `OCI_BUILD_REPOSITORY_ROOT != OCI_PUBLISH_REPOSITORY_ROOT`
- `OCI_PUBLISH_VOLATILE_IMAGES`

Pushes the volatile images from `OCI_BUILD_REPOSITORY_ROOT` to
`OCI_PUBLISH_REPOSITORY_ROOT` with the shortened git-tag as image tag.

### Job `oci:publish:branch`

- Runs on branch and merge request pipelines
- Runs only, when the branch name is a valid image tag

Pushes the volatile images from `OCI_BUILD_REPOSITORY_ROOT` to
`OCI_PUBLISH_REPOSITORY_ROOT` with the branch name as image tag.

### Job `oci:publish:tag`

- Runs on tag pipelines
- Runs only, when the tag name is a valid image tag

Pushes the volatile images from `OCI_BUILD_REPOSITORY_ROOT` to
`OCI_PUBLISH_REPOSITORY_ROOT` with the branch name as image tag.

## Notes

- If the image with the shortened `sha`-hash exists in the registry, the image
  will not be build a new. It is expected, that every build process is
  reproducible and immutable.
- By default, no image tag will be deleted by the workflow.
- Some filenames may lead to collisions in the image name. It is up to the user
  to avoid them.i
