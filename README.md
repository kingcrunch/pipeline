# DevOps Pipeline platform

## Quickstart

```yaml
include:
- project: kingcrunch/pipeline
  ref: main # Choose version (see below)
  file: default.yml
```

## Versions

Choose wisely. This projects follows [Semver](https://semver.org/).

- `main` follows the latest developments. You shouldn't use it.
- `vX.Y` branches follow the development of certain mainlines.
  Forwarding-breaking changes will increase `Y`.
- `vX.Y.Z` tags are fixated to a specific commit. Upgrades require manual
  interaction.

Projects are encouraged to follow the latest `vX.Y` branch and keep their `ref`
up to date. Although the tags provide the most stability, branches gets the
latest fixes automatically.

## Description

Jobs are grouped into "features". Features are enabled by common
feature-specific properties, like configuration files, or files of certain
dependency management tools.

### Workflow

When enabled, most features defines jobs, which integrates into the common
workflow

- Branch-, or Merge-Request-Pipeline
- Tag-Pipeline

The difference between branch- and merge-request-pipelines is, whether
an open merge request exists for the branch. Merge-Request-Pipelines are much
stricter.

A feature can define additional (manual) jobs.

## Variables

- `PIPELINE_IMAGE_REPOSITORY_ROOT`: Allows to specify a different repository to
  fetch the pipeline images from. To speed up the pipeline, one can set this
  to a registry near the runners. This registry must provide the same images.

## Available Features

- [`oci`](docs/oci.md): Builds and tags oci compatible images
- [`editorconfig`](docs/editorconfig.md)
- [`golang`]
- [`hugo`]
- [`pages`]
- [`renovate`](docs/renovate.md): Provides renovate job for manual, external, or
  schedule execution
- [`sonarscanner`]
- [`yaml`](docs/yaml.md): YAML-relevant linters
