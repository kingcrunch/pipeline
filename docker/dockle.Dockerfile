FROM docker.io/goodwithtech/dockle:v0.4.14

# Better CI suitable if it doesn't unconditionally invoke the tool directly
ENTRYPOINT [""]
CMD [""]

USER 11001:11001
