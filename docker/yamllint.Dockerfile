FROM docker.io/library/alpine:3.20.3

RUN echo "@edge-main https://dl-cdn.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories &&\
    echo "@edge-community https://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories &&\
    echo "@edge-testing https://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories &&\
    adduser --uid 11001 --home /home/nonroot --disabled-password nonroot

WORKDIR /home/nonroot

RUN apk add --no-cache \
    # renovate origin=alpine_3_20
    yamllint=1.35.1-r1

USER nonroot:nonroot
