FROM docker.io/library/alpine:3.20.3

RUN echo "@edge-main https://dl-cdn.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories &&\
    echo "@edge-community https://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories &&\
    echo "@edge-testing https://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories &&\
    adduser --uid 11001 --home /home/nonroot --disabled-password nonroot

WORKDIR /home/nonroot

RUN apk add --no-cache \
    # renovate origin=alpine_3_20
    hugo=0.125.4-r3

USER nonroot:nonroot
