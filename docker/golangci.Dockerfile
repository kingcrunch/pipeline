FROM docker.io/golangci/golangci-lint:v1.62.2

RUN groupadd --gid 11001 nonroot &&\
    useradd --uid 11001 --gid 11001 -m nonroot

WORKDIR /home/nonroot

USER nonroot:nonroot
